**<h1>EMOVIBE MUSIC🎶 (Music Recomendation Based On Face Expression 😐)--PRJ303 </h1>**
<h2>Purpose</h2>
<p>
The purpose of this project is to develop a personalized music
recommendation system based on facial expression recognition.
Music plays a significant role in our lives and has the ability
to evoke various emotions. However, finding the right music that
matches our current emotional state can be challenging. 
Traditional music recommendation systems often rely on user behavior
and past preferences, which may not accurately reflect the user's 
current emotional state. To address this challenge, our project aims
to utilize facial expression detection and machine learning techniques
to accurately determine a user's emotional state and provide personalized
music recommendations that align with their mood.
</p>

<h2>Features</h2>
<ol>
<li>User Input:Take the image of the face in the form of a 48 * 48 pixel format.
Face Expression Recognition: The system should have a mechanism to recognize the emotion of the user based on   their input.
</li>
<li>Face Expression Recognition: The system should have a mechanism to recognize the emotion of the user based on their input.</li>
<li>Music Recommendation: The system should recommend music based on the user's emotions.</li>
<li>Music Selection: The system should have a wide selection of music to choose from, including different genres and artists.</li>
<li>Play Music: Once the music is recommended based on facial expression, users can now play the music.</li>
<li>Music Search: The system should be able to search through a music database to find suitable songs based on the user's emotions.
</li>
<li>User Feedback: The system should provide a mechanism for the user to provide feedback on the recommended songs.</li>
</ol>
<h2>Technologies Used</h2>
<ul>
<li>Python</li>
<li>Django</li>
<li>HTML,CSS,JS & Boostrap</li>
<li>Open CV</li>
<li>Tensorflow</li>
<li>Keras</li>
</ul>
<h2>CCN Model Architecture</h2>
<P>Our model is built using the CNN algorithm which is a type of artificial neural network widely used for image/object recognition and classification. We have created a model using the CNN algorithm because it is widely used when the input data is images. And since our input data is the facial expression images, we decided the CNN algorithm was the best choice.
 
CNN is particularly good at learning features that are relevant to image classification tasks. They have the ability to extract features from the image through a process called convolution. Convolutional layers in a CNN apply a series of filters to an input image, identifying patterns and features that are important for classification. This allows the network to learn complex and abstract representations of the input data, which is particularly useful for detecting subtle variations in facial expressions. Another advantage of CNNs is their ability to handle variations in input size and position. Since facial expressions can vary greatly in size, orientation, and position within an image, CNNs are able to detect these expressions regardless of their specific location within the image.
</p>
<img src="Project Screenshots/Screenshot_2023-06-08_154045.png"/>
<br/>
<img src="Project Screenshots/Screenshot_2023-06-08_154103.png"/>
<h2>Installation and Usage</h2>
<p>To install and run the application locally, follow these steps:</p>
<ol>
    <li>Clone the repository: `git clone https://gitlab.com/wangchuk77jr/prj303.git`</li>
    <li>Set up the environment variables: `environment name -prj303ENV`</li>
    <li>Activate the vitual eniviroment: `prj303ENV\Scripts\activate`</li>
    <li>Install dependencies: `pip install`</li>
    <li>Start the development server: `python manage.py runserver`</li>
</ol>
<h2>System Functionalities Screenshots</h2>
<p>Home</p>
<img src="Project Screenshots/home.png"/>
<p>About</p>
<img src="Project Screenshots/about.png"/>
<p>Contact</p>
<img src="Project Screenshots/contact.png"/>
<p>Feedback</p>
<img src="Project Screenshots/feedback.png"/>
<p>Footer</p>
<img src="Project Screenshots/footer.png"/>
<p>Expression detection form</p>
<img src="Project Screenshots/detectionFrom.png"/>
<p>Music Recemendation</p>
<img src="Project Screenshots/recommededMusic.png">

<h2>Team Members</h2>
<p>The project is being conducted by a team of five members pursuing a Bachelor of Science in Information Technology at Gyalpozhing College of Information Technology. The team members are:</p>
<ol>
    <li> Sonam Wangchuk (12200084) - Team Lead, Backend Developer, and UI/UX Designer</li>
    <li>Pema Chophel (12200069) - Frontend Developer and Quality Assurance Engineer</li>
    <li>Pema Wangmo (12200073) - Quality Assurance Engineer and DevOps Engineer</li>
    <li>Purna Kumar Limbu (12200075) - Data Scientist and DevOps Engineer</li>
    <li>Sujata Rai (12200086) - Data Scientist and UI/UX Designer</li>
</ol>
<h2>Project Guide:</h2>
<p>
Ms. Ugyen Choden, an Associate Lecturer at Gyalpozhing College of Information Technology, provided invaluable guidance and support throughout the project. Her expertise and experience in the field of information technology shaped the project's direction and helped the team overcome challenges. Her vital guidance led to the successful completion of the project, and the team is grateful for her support.
</p>




